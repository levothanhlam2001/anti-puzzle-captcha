﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Auto_Tiktok.Helper
{
    public  class CaptchaResolve
    {
        public static Point Resolve(Bitmap src) {


            //convert to gray color
            src.Save("captcha.png");
            var main = src.ToImage<Bgr, byte>();
            var img = src.ToImage<Gray, byte>();
            img.Save("gray.png");

            var blur = new Image<Gray,byte>(img.Width, img.Height);
            CvInvoke.GaussianBlur(img, blur, new System.Drawing.Size(3, 3), 0);
            blur.Save("blur.png");
            var canny = new Image<Gray, byte>(img.Width, img.Height);
            CvInvoke.Canny(blur, canny, 700, 400);
            canny.Save("canny.png");
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hier = new Mat();
            CvInvoke.FindContours(canny, contours, hier, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            //List<Point> points = new List<Point>();
            for (int i = 0; i<contours.Size; i++)
            {
                var contour = contours[i];
                VectorOfPoint approx = new VectorOfPoint();
                double perimeter = CvInvoke.ArcLength(contour, true);
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.04 * perimeter, true);
                
                // Check when edge number from 4 to 8 and perimeter > 200
                if (approx.Size >=4 && approx.Size <= 8 && perimeter > 200)
                {
                    CvInvoke.DrawContours(main, contours, i, new MCvScalar(0, 0, 255));
                    
                    var momment = CvInvoke.Moments(contour);
                    int cX = (int)Math.Round(momment.M10 / momment.M00);
                    int cY = (int)Math.Round(momment.M01 / momment.M00);

                    // Check X coordinate of center verification image's larger than first then return point of verify
                    if (cX > 72)
                    {
                        CvInvoke.PutText(main, Math.Round(perimeter).ToString(),new Point(cX,cY), Emgu.CV.CvEnum.FontFace.HersheyTriplex, 1.0, new MCvScalar(0, 0, 255), 2);
                        main.Save("find.png");
                        return new Point(cX, cY);
                    }
                }
            }

            return new Point();
        }
    }
}
